#!/bin/bash
#: Title   : Lua Reference Manual Convertor
#: Date    : 2012-01-28 15:09:48 CET
#: Author  : "Milan Banjac" <tap3ahchina@gmail.com>
#: Licence : GPLv3

#############################################################################
# Check for dependencies                                                    #
#############################################################################
fine=$(expr "$( echo $BASH_VERSION | sed 's/^\([0-9]\+\?\)\..*$/\1/g' )" == 4)
if [ $fine != "1" ]; then
	echo "You need bash version 4"
fi
for d in convert date expr grep makeindex pdflatex rm sed tidy wget; do
	hash "$d" 2> /dev/null
	if [ "$?" != "0" ]; then
		echo "Dependency missing: $d"
		fine=0
	fi
done
if [ $fine != "1" ]; then
	exit
fi

IFS=$(echo -en "\n\b")

#############################################################################
# Fetch references and store them into references[], also get the date      #
#############################################################################
declare -A references
while read -r line; do
	if [ -n "$( echo "$line" | grep '<h3' )" ] && [ "$line" != "<h3>&nbsp;</h3>" ]; then
		i=$(( ${i:=0} + 1 ))
		topindex="$i@{\\large\\bfseries $( echo "$line" | sed 's/<a [^>]*>//g' | sed 's/<\/a>//g' | sed 's/<h3>//g' | sed 's/<\/h3>//g' )}"
	elif [ -n "${topindex:=""}" ] && [ -n "$( echo "$line" | grep '<a href' )" ]; then
		references[$( echo "$line" | sed 's/.\{0,3\}<a href="manual\.html#\([^"]*\)">[^<]*<\/a><br \/>.\{0,4\}/\1/g' )]="$topindex!\\texttt{$( echo "$line" | sed 's/.\{0,3\}<a href="manual\.html#[^"]*">\([^<]*\)<\/a><br \/>.\{0,4\}/\1/g' )}"
	fi
done < <( wget -O- "http://www.lua.org/manual/5.2/contents.html" | tidy -q -w 0 --hide-comments yes --show-warnings no -asxhtml )

#############################################################################
# Fetch lua logo picture                                                    #
#############################################################################
wget -nc "http://www.lua.org/images/lua.gif"
convert "lua.gif" "lua.png"
rm -f "lua.gif"

#############################################################################
# Process HTML text                                                         #
#############################################################################
(
echo "\\documentclass[a4paper]{book}"
echo
echo "\\usepackage[T1]{fontenc}"
echo "\\usepackage{makeidx}"
echo "\\usepackage{lmodern}"
echo "\\usepackage{alltt}"
echo "\\usepackage[top=2cm, bottom=2cm, left=2cm, right=2.5cm]{geometry}"
echo "\\usepackage{graphicx}"
echo
echo "\\addtolength{\\topmargin}{-.3cm}"
echo "\\addtolength{\\headsep}{.3cm}"
echo "\\makeindex"
echo
echo "\\begin{document}"
echo
echo "\\begin{titlepage}"
echo
echo "\\begin{center}"
echo "\\includegraphics[width=0.25\\textwidth]{lua.png}\\\\[2cm]"
wget -O- "http://www.lua.org/manual/5.2/manual.html" |
	tidy -q -w 0 --hide-comments yes --show-warnings no -asxhtml |
	while read -r line; do
		if [ -n "$( echo "$line" | grep '<img ' )" ]; then
			title="$( echo "$line" | sed 's/<h1><a [^>]\+><img [^>]\+><\/a> *\([^<]*\)<\/h1>.*/\1/g' )"
			read -r line
			echo "{\\large $( echo "$line" | sed 's/^by //g' | sed 's/\([A-Za-z]*\), /\\textsc{\1}\\hfill /g' | sed 's/\([A-Za-z]*\)$/\\textsc{\1}/g' )}\\\\[0.5cm]" | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
			echo "\\rule{\\linewidth}{0.5mm}\\\\[0.7cm]"                                                                                                             | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
			echo "{\\Huge\\bfseries $title}\\\\[0.4cm]"                                                                                                              | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
			echo "\\rule{\\linewidth}{0.5mm}"                                                                                                                        | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
			echo "\\vfill"                                                                                                                                           | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
			continue
		elif [ -n "$( echo "$line" | grep '<small' )" ]; then
			if [ -n "$( echo "$line" | grep '<a' )" ]; then
				echo "{\\normalsize $( echo "$line" | sed 's/<[^>]*>//g' )}\\\\[1cm]"                                                                                  | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo "{\\large @@@}"                                                                                                                                   | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo "\\end{center}"                                                                                                                                   | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo
				echo "\\end{titlepage}"                                                                                                                                | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo
				echo "\\thispagestyle{empty}"                                                                                                                          | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo "\\cleardoublepage"                                                                                                                               | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo "\\pagenumbering{roman}"                                                                                                                          | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo "\\tableofcontents"                                                                                                                               | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo "\\cleardoublepage"                                                                                                                               | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo "\\pagenumbering{arabic}"                                                                                                                         | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo "\\setcounter{page}{1}"                                                                                                                           | sed 's/\\/\&tempbksl;/g' | sed 's/{/\&tmpcrlyl;/g' | sed 's/}/\&tmpcrlyr;/g'
				echo
			else
				date -u -d "$( echo "$line" | sed 's/<[^>]*>//g' | sed 's/^Last update: //g' )" "+%d. %B %Y, %T (%Z)" > temp
				break
			fi
		elif [ -n "$( echo "$line" | grep '<h1' )" ]; then
			on=1
		fi
		if [ ${on:=0} -eq 1 ] && [ "$line" != "<hr />" ] && [ "$line" != "<p></p>" ]; then
			echo "$line"
		fi
	done |
	sed 's/\(<h[0-9]>\)[0-9]\+\(\.[0-9]\+\)* &ndash; <a name="/\1<a name="/g;
	s/<\/a>//g;
	s/\(<a[^>]*\) id="[^"]*"\([^>]*>\)/\1\2/g;
	s/<a href="[^"]*">//g;
	s/\(<h[0-9]>\)\(<a name="[^"]*">\)/\2\1/g;
	s/<p>//g;
	s/<\/p>/\n/g;
	s/{/\&tempbksl;\&tmpcrlyl;/g;
	s/}/\&tempbksl;\&tmpcrlyr;/g;
	s/\\/\\textbackslash{}/g;
	s/&tempbksl;/\\/g;
	s/&tmpcrlyl;/{/g;
	s/&tmpcrlyr;/}/g;
	s/&amp;/\\\&/g;
	s/#/\\#/g;
	s/\$/\\$/g;
	s/%/\\%/g;
	s/~/\\textasciitilde{}/g;
	s/&nbsp;/~/g;
	s/\^/\\textasciicircum{}/g;
	s/&sect;/\\S{}/g;
	s/&middot;&middot;&middot;/\\dots{}/g;
	s/|/\\textbar{}/g;
	s/&lsquo;/`/g;
	s/--/-{}-/g;
	s/&ndash;/--/g;
	s/&copy;/\\copyright{}/g' |
	sed "s/&rsquo;/'/g" |
	while read -r line; do
		OIFS=$IFS
		IFS='<'
		line2=
		for word in $line; do
			pos=$( expr index "$word" '>' )
			if [ $pos -ne 0 ]; then
				if [ ${word:0:8} == "a name=\"" ]; then
					indx=${word:8:(( $pos - 10 ))}
					word=${word:$pos:(( ${#word} - $pos ))}
					if [ ${references[$indx]} ]; then
						word="\\index{${references[$indx]}}$word"
					fi
				else
					word="<$word"
				fi
			fi
			line2+=$word
		done
		IFS=$OIFS
		echo "$line2"
	done |
	sed 's/_/\\_/g;
	s/\([-+A-Za-z0-9.]*\)<sup>\([^<]*\)<\/sup>/$\1^{\2}$/g;
	s/<code>/\\texttt{/g;
	s/<\/code>/}/g;
	s/<b>/\\textbf{/g;
	s/<\/b>/}/g;
	s/<em>/\\textit{/g;
	s/<\/em>/}/g;
	s/<h1>/\\chapter{/g;
	s/<\/h1>/}/g;
	s/<h2>/\\section{/g;
	s/<\/h2>/}/g;
	s/<h3>/\\subsection{/g;
	s/<\/h3>/}/g;
	s/<h4>/\\subsubsection{/g;
	s/<\/h4>/}/g;
	s/&le;/$\\le$/g;
	s/&pi;/$\\pi$/g;
	s/<ul>/\\begin{itemize}/g;
	s/<\/ul>/\\end{itemize}/g;
	s/<li>/\\item /g;
	s/<\/li>//g;
	s/<pre>/\\begin{alltt}/g;
	s/<\/pre>/\\end{alltt}/g;
	s/<span class="apii">\([^<]*\)<\/span>/\\hfill{}\1/g;
	s/&gt;/\\textgreater{}/g;
	s/&lt;/\\textless{}/g'

echo
echo "\\cleardoublepage"
echo "\\addcontentsline{toc}{chapter}{Index}"
echo "\\printindex"
echo
echo "\\end{document}"
) | sed "s/@@@/$( cat temp )/g" > lua.tex
rm -f temp

pdflatex lua.tex > /dev/null
pdflatex lua.tex > /dev/null
makeindex lua > /dev/null
pdflatex lua.tex > /dev/null
pdflatex lua.tex > /dev/null
rm -f lua.aux lua.idx lua.ilg lua.ind lua.log lua.toc

